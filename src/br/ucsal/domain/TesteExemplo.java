package br.ucsal.domain;


/*Classe foi adicionada APENAS para TESTE e n�o faz Parte das classes de DOMINIO
 *Portanto DESCONSIDERE esta classe*/

public class TesteExemplo {

	public static void main(String[] args) {
		
		//Adiciona 2 itens ao cardapio (Codigo, Nome, Valor)
		Item item01 = new Item("B01","Coxinha",8.00);
		Item item02 = new Item("B02","Suco",10.00);

		//adicionar nova mesa (Numero da Mesa,Quantidade de cadeiras, Localiza��o)
		Mesa mesa01 = new Mesa(1,4,"Interna");

		//Cria um nova comanda (Numero da Comanda, Numero de clientes, A comanda foi encerrada ? , mesa associada a comanda)
		Comanda comanda01 = new Comanda(1,4,false,mesa01);
		
		//Relalizar dois pedidos (Item , quantidade)
		Pedido pedido01 = new Pedido(item01 , 5);
		Pedido pedido02 = new Pedido(item02 , 2);
		
		//Adiciona os pedidos a comanda (Pedido)
		comanda01.pedidos.add(pedido01);
		comanda01.pedidos.add(pedido02);

		
		mostrarComanda(comanda01);
		
	}
	//Imprime a comanda
	private static void mostrarComanda(Comanda comanda) {
		System.out.println("N�mero: " + comanda.numComanda);
		System.out.println("Comanda Encerrada :" + comanda.comandaEncerrada);
		System.out.println("Quatidade de Clientes " + comanda.qtClientes);
		mostraItem(comanda);
		System.out.print("Mesa N�mero: " + comanda.mesa.numMesa);
		System.out.print(", Localiza��o: " + comanda.mesa.localMesa);
		System.out.print(", Quatidade de cadeira: " + comanda.mesa.qtCadeiras);
	}
	
	
	//Imprime a lista de Pedidos 
	private static void mostraItem(Comanda comanda) {
		for(Pedido pedido : comanda.pedidos) {
			System.out.print("Itens: " + pedido.qtItem + " unidades de ");
			System.out.print("[ C�digo: " + pedido.item.codigo);
			System.out.print(", Nome: " + pedido.item.nome);
			System.out.println(" ,Valor: " + pedido.item.valor +" ]");
		}
	}

}
