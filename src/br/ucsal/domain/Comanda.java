package br.ucsal.domain;

import java.util.ArrayList;
import java.util.List;

public class Comanda {
	
	int numComanda , qtClientes;
	boolean comandaEncerrada;
	Mesa mesa;
	
	/* !!PONTOS DE ATEN��O!!
	 * "Os objetos de item devem estar ligados aos objetos de comanda (n�o basta guardar o c�digo do item)."
	 * 
	 * Para associar os itens a comanda juntamente com o numero de itens solicitados criei uma nova classe pedidos 
	 * que tem como atributos o ITEM (N�O APENAS SEU CODIGO) e a quatidade solicitada do item especifico.
	 * 
	 *       COMANDA -> PEDIDOS -> (ITEM && Quantidade de Itens)*/
	
	List<Pedido> pedidos = new ArrayList<> ();
	
	
	public Comanda(int numComanda, int qtClientes, boolean comandaEncerrada, Mesa mesa) {

		this.numComanda = numComanda;
		this.qtClientes = qtClientes;
		this.comandaEncerrada = comandaEncerrada;
		this.mesa = mesa;
	}
}
